<?php
// File json yang akan dibaca (full path file)
$file = "unit.json";

// Mendapatkan file json
$unit = file_get_contents($file);

// Mendecode anggota.json
$data = json_decode($unit, true);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Simulasi Kredit | Tokoweb</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link id="favicon" rel="shortcut icon" href="assets/favicon.png" type="image/png" />
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="container text-center">
		<div class="form-box">
			<h2>Simulasi Kredit Kendaraan Mobil</h2>
			<form id="waform" action="#" method="GET">
				<div class="form-group">
					<select id="unit-mobil" class="form-input">
						<option disabled selected>Tipe Mobil</option>
						<?php
							// Membaca data array menggunakan foreach
							foreach ($data as $d)  : ?>
								<option value="<?php echo $d['TYPE'].' - '.$d['HARGA'] ?>"><?php echo $d['TYPE'].' - '.$d['HARGA'] ?></option>
						<?php
							endforeach;
						?>
					</select>
				</div>
				<div class="form-group">
					<select id="tenor" class="form-input">
						<option disabled selected>Pilih Tenor</option>
						<option>1 Tahun</option>
						<option>2 Tahun</option>
						<option>3 Tahun</option>
						<option>4 Tahun</option>
						<option>5 Tahun</option>
					</select>
				</div>
				<div class="form-group">
					<input type="number" id="uang-muka" class="form-input" placeholder="Uang Muka">
				</div>
				<div class="form-group">
					<input type="text" id="nama" class="form-input" placeholder="Nama Lengkap Anda">
				</div>
				<div class="form-group">
					<input type="text" id="hp" class="form-input" placeholder="No HP / WhatsApp">
				</div>
				<div class="form-group">
					<button type="submit" class="button">HITUNG HARGA KREDIT</button>
				</div>
			</form>
		</div>
	</div>
	<div class="wa-bg"></div>

	<script type="text/javascript">
		var WaForm = {
			init: function() {
				this.getForm().addEventListener('submit', this.submit);
			},
			getForm: function() {
				return document.getElementById('waform');
			},
			submit: function( event ) {
				event.preventDefault();
				var msg = WaForm.getMsg(
						'Tipe Mobil : '+ document.getElementById('unit-mobil').value +'\n'+
						'Tenor : '+ document.getElementById('tenor').value +'\n'+
						'Uang Muka : '+ document.getElementById('uang-muka').value +'\n'+
						'Nama Lengkap : '+ document.getElementById('nama').value +'\n'+
						'No HP / WA : '+ document.getElementById('hp').value),
					phone = '62xxxxxx';

				if (WaForm.isMobile()) {
					window.open('whatsapp://send?phone=' + phone + '&text=' + msg, '_blank');
				} else {
					window.open('https://wa.me/' + phone + '?text=' + msg, '_blank');
				}
			},
			getMsg: function( text ) {
				return encodeURIComponent(text);
			},
			isMobile: function() {
				return navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i);
			}
		};
		WaForm.init(); // init app
	</script>
</body>
</html>